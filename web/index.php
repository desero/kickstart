<?php

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Loader\YamlFileLoader;

use Symfony\Component\Yaml\Parser;
$yaml = new Parser();
$config = $yaml->parse(file_get_contents(__DIR__.'/../settings.yml'));

$app = new Silex\Application();
$app['debug'] = true;

$app['swiftmailer.options'] = array(
    'host' => 'localhost',
    'port' => '25',
    'username' => 'kickstart',
    'password' => 'kickstart',
    'encryption' => null,
    'auth_mode' => null
);

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app['session']->start();

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
));
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

$app['app_default_locale'] = $config['default_language'];
$app['app_allowed_locales'] = array_keys($config['languages']);

$locale = function() use ($app) {
  $path = $_SERVER['REQUEST_URI'];
  $request_tokens = explode('/', $path);
  $locale = in_array($request_tokens[1], $app['app_allowed_locales']) ? $request_tokens[1] : $app['app_default_locale'];
  $localeFromSession = $app['session']->get('locale');
  if($locale != $localeFromSession) {
    $app['session']->set('locale', $locale);
  }
  return $locale;
};

$app->register(new Silex\Provider\TranslationServiceProvider(), array(
  'locale' => $locale,
  'locale_fallback' => $app['app_default_locale'],
));

$app->register(new Umpirsky\Silex\I18nRouting\Provider\I18nRoutingServiceProvider());

$settings = array();
foreach ($config['routes'] as $lang => $set) {
	foreach ($set as $page => $data) {
		$slug = $config['routes'][$app['app_default_locale']][$page]['path'];
		$settings['routes'][$lang][$slug] = $data['path'];
		$settings['menu'][$lang][$slug] = $data['title'];
	}
}
$app['translator.domains'] = $settings;

$app['translator'] = $app->share($app->extend('translator', function ($translator, $app) {
  $translator->addLoader('yaml', new YamlFileLoader());
  $translator->addResource('yaml', __DIR__ . '/../locales/ro.yml', 'ro');
  $translator->addResource('yaml', __DIR__ . '/../locales/en.yml', 'en');
  return $translator;
}));

$app->before(function (Request $request) use ($app, $settings, $config) {
    foreach ($app['app_allowed_locales'] as $language) {
    	$slug = ($request->get('_route') !== "index") ? ("/" . $request->get('_route')) : "/";
    	$route = $settings['routes'][$language][$slug];
		$switcher[$language] = array(
			'path' => (($app['session']->get('locale') == $language) ? $request->getRequestUri() : $route),
			'title' => $config['languages'][$language]
		  );
    }
    $app['twig']->addGlobal('switcher', $switcher);
    $app['twig']->addGlobal('routes', $settings['routes'][$app['session']->get('locale')]);
    $app['twig']->addGlobal('menu', $settings['menu'][$app['session']->get('locale')]);
});

// Frontpage
$app->get('/', function () use ($app) {
    return $app['twig']->render('hello.twig', array(
        'name' => 'home',
    ));
})->bind('index');

$app->post('/', function () use ($app) {
    $request = $app['request'];

    $message = \Swift_Message::newInstance()
        ->setSubject('[YourSite] Feedback')
        ->setFrom(array('noreply@localhost'))
        ->setTo(array('emailaddress@mail.com'))
        ->setBody($request->get('message'));

    $app['mailer']->send($message);

    return $app['twig']->render('sent.twig');
});

// News
$app->get('/stiri', function () use ($app) {
  return $app['twig']->render('stiri.twig');
})->bind('stiri');

// Archive
$app->get('/arhiva', function () use ($app) {
  return $app['twig']->render('stiri.twig');
})->bind('arhiva');

$app->run();