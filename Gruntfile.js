module.exports = function(grunt) {
  'use strict';

  grunt.initConfig({

    // Watcher
    watch: {
      sass: {
        files: ['src/sass/**/*.{scss,sass}'],
        tasks: ['compile', 'minify']
      },
      js: {
        files: ['src/js/**/*.js'],
        tasks: ['copy:js']
      },
      html: {
        files: ['src/html/**/*.html'],
        tasks: ['copy:html']
      },
      images: {
        files: ['src/images/**/*'],
        tasks: ['copy:images']
      },
      livereload: {
        files: ['*.html', '*.php', 'js/**/*.{js,json}', 'css/*.css', 'img/**/*.{png,jpg,jpeg,gif,webp,svg}'],
        options: {
          livereload: true
        }
      }
    },

    // SASS Compiler
    sass: {
      dist: {
        files: {
          'web/css/kickstart.css': 'src/sass/kickstart.scss'
        }
      }
    },
    
    // Assets
    copy: {
      fontawesome: {
         cwd: 'bower_components/fontawesome/fonts',
         src: '**/*',
         dest: 'web/fonts/',
         expand: true
      },
      images: {
         cwd: 'src/images',
         src: '**/*',
         dest: 'web/images/',
         expand: true
      },
      fonts: {
         cwd: 'src/fonts',
         src: '**/*',
         dest: 'web/fonts/',
         expand: true
      },
      css: {
        src: ['bower_components/normalize.css/normalize.css', 'bower_components/owl-carousel2/dist/assets/owl.carousel.min.css', 'bower_components/owl-carousel2/dist/assets/owl.theme.default.min.css'],
        dest: 'web/css/',
        flatten: true,
        expand: true
      },
      html: {
        cwd: 'src/html',
        src: '**/*.html',
        dest: 'web/',
        expand: true
      },
      js: {
        src: [
          'bower_components/jquery/dist/jquery.min.js',
          'bower_components/jquery/dist/jquery.min.map',
          'bower_components/modernizr/modernizr.js',
          'bower_components/owl-carousel2/dist/owl.carousel.min.js',
          'src/js/kickstart.js',
          ],
        dest: 'web/js/',
        flatten: true,
        expand: true
      }
    },

    // CSS minifier
    cssmin: {
        combine: {
            files: {
                'web/css/kickstart.min.css': ['web/css/kickstart.css']
            }
        }
    },

  });

  // Register modules
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Register tasks
  grunt.registerTask('compile', ['sass:dist']);
  grunt.registerTask('minify', ['cssmin:combine']);
  grunt.registerTask('assets', ['copy:html', 'copy:fontawesome', 'copy:images', 'copy:fonts', 'copy:js', 'copy:css']);
  grunt.registerTask('build', ['compile', 'minify', 'assets']);
  grunt.registerTask('default', ['build', 'watch']);

};
